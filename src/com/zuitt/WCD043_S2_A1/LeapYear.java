package com.zuitt.WCD043_S2_A1;

import java.util.Scanner;

public class LeapYear {
    public static void main(String[] args) {

        Scanner numScan = new Scanner(System.in);
        System.out.println("Input year to be checked if a leap year.");

        int yearToCheck = numScan.nextInt();

        if (yearToCheck % 4 == 0 && yearToCheck % 100 != 0 || yearToCheck % 400 == 0) {
            System.out.println(yearToCheck + " is a leap year");
        } else {
            System.out.println(yearToCheck + " is NOT a leap year");
        }



    }
}
