package com.zuitt.WCD043_S2_A2;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Scanner;

public class PrimeNumber {
    public static void main(String[] args) {

        int[] primeNumber = {2, 3, 5, 7, 11};
        System.out.println(Arrays.toString(primeNumber));
         System.out.println("The first prime numbers is: " + primeNumber[0]);

//        Scanner input = new Scanner(System.in);
//        int index = input.nextInt();
//        System.out.println("The first prime numbers is: " + primeNumber[index]);

//        String[] friends = {"John", "Jane", "Chloe", "Zoey"};
//        System.out.println("My friends are: " + Arrays.toString(friends));

        ArrayList<String> friends = new ArrayList<String>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));
        System.out.println("My friends are: " + friends);
        HashMap<String, String> inventory = new HashMap<String, String>() {
            {
                put("toothpaste", "15");
                put("toothbrush", "20");
                put("soap", "12");
            }
        };
        System.out.println("Our current inventory consists of: " + inventory);

    }
}
